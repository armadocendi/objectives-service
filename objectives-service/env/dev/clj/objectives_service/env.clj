(ns objectives-service.env
  (:require
    [selmer.parser :as parser]
    [clojure.tools.logging :as log]
    [objectives-service.dev-middleware :refer [wrap-dev]]))

(def defaults
  {:init
   (fn []
     (parser/cache-off!)
     (log/info "\n-=[objectives-service started successfully using the development profile]=-"))
   :stop
   (fn []
     (log/info "\n-=[objectives-service has shut down successfully]=-"))
   :middleware wrap-dev})
